package com.metadoor.assigment3;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class NewOfficeActivity extends Activity implements View.OnClickListener {

    private EditText nameField;
    private EditText cityField;
    private EditText addressField;
    private EditText phoneField;
    private EditText zipcodeField;
    private Button saveButton;
    private Button cancelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_office);

        nameField       = (EditText) findViewById(R.id.nameField);
        cityField       = (EditText) findViewById(R.id.cityField);
        addressField    = (EditText) findViewById(R.id.addressField);
        phoneField      = (EditText) findViewById(R.id.phoneField);
        zipcodeField    = (EditText) findViewById(R.id.zipcodeField);
        saveButton      = (Button) findViewById(R.id.saveButton);
        cancelButton    = (Button) findViewById(R.id.cancelButton);

        saveButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
    }

    public void goBack() {
        Intent intent = new Intent(this, TabBarActivity.class);
        startActivity(intent);
        finish();
    }

    public void saveOffice() {
        Uri uri = DBConstants.CONTENT_URI_LOCATIONS;
        ContentResolver cr = getContentResolver();
        ContentValues values = new ContentValues();

        values.put(DBConstants.LOCATION_ADDRESS, addressField.getText().toString());
        values.put(DBConstants.LOCATION_CITY, cityField.getText().toString());
        values.put(DBConstants.LOCATION_NAME, nameField.getText().toString());
        values.put(DBConstants.LOCATION_PHONE, phoneField.getText().toString());
        values.put(DBConstants.LOCATION_ZIPCODE, zipcodeField.getText().toString());

        cr.insert(uri, values);

        goBack();
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.cancelButton) goBack();
        if(view.getId() == R.id.saveButton) saveOffice();
    }
}
