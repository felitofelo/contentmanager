package com.metadoor.assigment3;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TextView;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;

public class TabBarActivity extends TabActivity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        TabHost tabHost = getTabHost();
        TabHost.TabSpec spec;
        Intent intent;

        intent = new Intent().setClass(this, ShowCompanyInfo.class);
        spec = tabHost.newTabSpec("Company").setIndicator("Company")
                .setContent(intent);
        tabHost.addTab(spec);

        intent = new Intent().setClass(this, ShowCompanyLocation.class);
        spec = tabHost.newTabSpec("Offices").setIndicator("Offices")
                .setContent(intent);
        tabHost.addTab(spec);
    }
}
