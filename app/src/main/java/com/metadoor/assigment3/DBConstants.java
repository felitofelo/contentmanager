package com.metadoor.assigment3;

import android.content.UriMatcher;
import android.net.Uri;

/**
 * Created by metad00r on 15/12/15.
 */
public class DBConstants {

    public static final String AUTHORITY = "com.example.karen.assignment3";
    public static final String DB_INFO_TABLE = "info";
    public static final String DB_LOCATIONS_TABLE = "location";

    public static final String COMPANY_ID = "_id";
    public static final String COMPANY_NAME = "companyName";
    public static final String COMPANY_WEBSITE = "companyWebsite";
    public static final String COMPANY_HISTORY = "companyHistory";
    public static final String COMPANY_ADDRESS = "companyAddress";
    public static final String COMPANY_ZIPCODE = "companyZipCode";
    public static final String COMPANY_CITY = "companyCity";
    public static final String COMPANY_PHONE = "companyPhone";
    public static final String IMAGE1 = "image1";
    public static final String IMAGE2 = "image2";
    public static final String IMAGE3 = "image3";

    public static final String LOCATION_ID = "_id";
    public static final String LOCATION_NAME = "locationName";
    public static final String LOCATION_ADDRESS = "locationAddress";
    public static final String LOCATION_ZIPCODE = "locationZipCode";
    public static final String LOCATION_CITY = "locationCity";
    public static final String LOCATION_PHONE = "locationPhone";

    public static final int SINGLE_ROW_LOCATION = 2;
    public static final int ALL_ROWS_LOCATION = 1;

    public static final int SINGLE_ROW_INFO = 6;
    public static final int ALL_ROWS_INFO = 12;

    public static final Uri CONTENT_URI_INFO = Uri.parse("content://" + AUTHORITY + "/" + DB_INFO_TABLE);
    public static final Uri CONTENT_URI_LOCATIONS = Uri.parse("content://" + AUTHORITY + "/" + DB_LOCATIONS_TABLE);
}
