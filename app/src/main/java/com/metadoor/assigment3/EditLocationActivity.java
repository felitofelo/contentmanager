
package com.metadoor.assigment3;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class EditLocationActivity extends Activity implements View.OnClickListener {

    private EditText nameField;
    private EditText cityField;
    private EditText addressField;
    private EditText phoneField;
    private EditText zipcodeField;
    private Button saveButton;
    private Button cancelButton;
    private Button deleteButton;
    private Office selectedOffice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_location);

        nameField = (EditText) findViewById(R.id.nameField);
        cityField = (EditText) findViewById(R.id.cityField);
        addressField = (EditText) findViewById(R.id.addressField);
        phoneField = (EditText) findViewById(R.id.phoneField);
        zipcodeField = (EditText) findViewById(R.id.zipcodeField);
        saveButton = (Button) findViewById(R.id.saveButton);
        cancelButton = (Button) findViewById(R.id.cancelButton);
        deleteButton = (Button) findViewById(R.id.deleteButton);
        saveButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        deleteButton.setOnClickListener(this);

        Intent intent = getIntent();
        int id = intent.getIntExtra("id", 0);
        String name = intent.getStringExtra("name");
        String address = intent.getStringExtra("address");
        String zip = intent.getStringExtra("zip");
        String city = intent.getStringExtra("city");
        String phone = intent.getStringExtra("phone");

        selectedOffice = new Office();
        selectedOffice.setId(id);
        selectedOffice.setName(name);
        selectedOffice.setAddress(address);
        selectedOffice.setCity(city);
        selectedOffice.setPhone(phone);
        selectedOffice.setZip(zip);

        nameField.setText(selectedOffice.getName());
        cityField.setText(selectedOffice.getCity());
        addressField.setText(selectedOffice.getAddress());
        phoneField.setText(selectedOffice.getPhone());
        zipcodeField.setText(selectedOffice.getZip());
    }

    public void goBack() {
        Intent intent = new Intent(this, TabBarActivity.class);
        startActivity(intent);
        finish();
    }

    public void saveOffice() {
        Uri uri = DBConstants.CONTENT_URI_LOCATIONS;
        ContentResolver cr = getContentResolver();
        ContentValues values = new ContentValues();
        String selectionClause = DBConstants.LOCATION_ADDRESS + " LIKE ?";
        String[] selectionArgs = {selectedOffice.getAddress()};

        values.put(DBConstants.LOCATION_ADDRESS, addressField.getText().toString());
        values.put(DBConstants.LOCATION_CITY, cityField.getText().toString());
        values.put(DBConstants.LOCATION_NAME, nameField.getText().toString());
        values.put(DBConstants.LOCATION_PHONE, phoneField.getText().toString());
        values.put(DBConstants.LOCATION_ZIPCODE, zipcodeField.getText().toString());

        cr.update(uri, values, selectionClause, selectionArgs);

        goBack();
    }

    public void deleteOffice(){
        Uri uri = DBConstants.CONTENT_URI_LOCATIONS;
        ContentResolver cr = getContentResolver();
        String selectionClause = DBConstants.LOCATION_ID + " = ?";
        String[] selectionArgs = {Integer.toString(selectedOffice.getId())};

        cr.delete(uri, selectionClause, selectionArgs);
        goBack();
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.saveButton) saveOffice();
        if(view.getId() == R.id.cancelButton) goBack();
        if(view.getId() == R.id.deleteButton) deleteOffice();
    }
}