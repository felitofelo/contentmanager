package com.metadoor.assigment3;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;


public class EditCompanyInfo extends Activity implements View.OnClickListener {

    private static final int SELECT_PHOTO = 100;
    private EditText companyNameTxt;
    private EditText companyWebsiteTxt;
    private EditText companyAddressTxt;
    private EditText companyZipTxt;
    private EditText companyCityTxt;
    private EditText companyPhoneTxt;
    private EditText companyHistoryTxt;

    private ImageView companyImage1;
    private ImageView companyImage2;
    private ImageView companyImage3;

    private int imageViewId = 0;

    private Button saveBtn;
    private Button cancelBtn;

    CompanyInfo ci = new CompanyInfo();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_company_info);

        companyNameTxt = (EditText) findViewById(R.id.companyNameTxt);
        companyWebsiteTxt = (EditText) findViewById(R.id.companyWebsiteTxt);
        companyAddressTxt = (EditText) findViewById(R.id.companyAddressTxt);
        companyZipTxt = (EditText) findViewById(R.id.companyZipTxt);
        companyCityTxt = (EditText) findViewById(R.id.companyCityTxt);
        companyPhoneTxt = (EditText) findViewById(R.id.companyPhoneTxt);
        companyHistoryTxt = (EditText) findViewById(R.id.companyHistoryTxt);
        companyImage1 = (ImageView) findViewById(R.id.imageView1);
        companyImage2 = (ImageView) findViewById(R.id.imageView2);
        companyImage3 = (ImageView) findViewById(R.id.imageView3);

        Intent intent = getIntent();
        fillEditContent(intent);

        saveBtn = (Button) findViewById(R.id.saveBtn);
        cancelBtn = (Button) findViewById(R.id.cancelBtn);
        saveBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
    }

    private void fillEditContent(Intent intent) {
        companyNameTxt.setText(intent.getStringExtra("COMPANY_NAME"));
        companyWebsiteTxt.setText(intent.getStringExtra("COMPANY_WEBSITE"));
        companyAddressTxt.setText(intent.getStringExtra("COMPANY_ADDRESS"));
        companyZipTxt.setText(intent.getStringExtra("COMPANY_ZIP"));
        companyCityTxt.setText(intent.getStringExtra("COMPANY_CITY"));
        companyPhoneTxt.setText(intent.getStringExtra("COMPANY_PHONE"));
        companyHistoryTxt.setText(intent.getStringExtra("COMPANY_HISTORY"));

        setCompanyImagesView(companyImage1, intent.getStringExtra("COMPANY_IMAGE1"));
        setCompanyImagesView(companyImage2, intent.getStringExtra("COMPANY_IMAGE2"));
        setCompanyImagesView(companyImage3, intent.getStringExtra("COMPANY_IMAGE3"));
    }

    private void setCompanyImagesView(ImageView companyImage, String absolutePath) {
        File image = new File(absolutePath);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(),bmOptions);

        companyImage.setImageBitmap(bitmap);

        switch (companyImage.getId()){
            case R.id.imageView1:
                ci.setImage1(absolutePath);
                break;
            case R.id.imageView2:
                ci.setImage2(absolutePath);
                break;
            case R.id.imageView3:
                ci.setImage3(absolutePath);
                break;
        }
    }

    private void updateCompany(View view)
    {
        String name = companyNameTxt.getText().toString();
        String website = companyWebsiteTxt.getText().toString();
        String address = companyAddressTxt.getText().toString();
        String zip = companyZipTxt.getText().toString();
        String city = companyCityTxt.getText().toString();
        String phone = companyPhoneTxt.getText().toString();
        String history = companyHistoryTxt.getText().toString();

        ci.setName(name);
        ci.setWebsite(website);
        ci.setAddress(address);
        ci.setZip(zip);
        ci.setCity(city);
        ci.setPhone(phone);
        ci.setHistory(history);

        ContentResolver cr = getContentResolver();
        ContentValues values = new ContentValues();

        values.put(DBConstants.COMPANY_NAME, ci.getName());
        values.put(DBConstants.COMPANY_WEBSITE, ci.getWebsite());
        values.put(DBConstants.COMPANY_ADDRESS, ci.getAddress());
        values.put(DBConstants.COMPANY_ZIPCODE, ci.getZip());
        values.put(DBConstants.COMPANY_CITY, ci.getCity());
        values.put(DBConstants.COMPANY_PHONE, ci.getPhone());
        values.put(DBConstants.COMPANY_HISTORY, ci.getHistory());
        values.put(DBConstants.IMAGE1, ci.getImage1());
        values.put(DBConstants.IMAGE2, ci.getImage2());
        values.put(DBConstants.IMAGE3, ci.getImage3());

        String selectionClause = DBConstants.COMPANY_ID + " LIKE ?";
        String[] selectionArgs = { "1" };

        int rowsUpdated = cr.update(
                DBConstants.CONTENT_URI_INFO,
                values,
                selectionClause,
                selectionArgs
        );

        Log.d("UPDATED: ", String.valueOf(rowsUpdated));
        goToCompanyInfo(ci, view);
    }

    private void goToCompanyInfo(CompanyInfo ci, View view)
    {
        Intent intent = new Intent(this, TabBarActivity.class);
        intent.putExtra("COMPANY_NAME", ci.getName());
        intent.putExtra("COMPANY_WEBSITE", ci.getWebsite());
        intent.putExtra("COMPANY_ADDRESS", ci.getAddress());
        intent.putExtra("COMPANY_ZIP", ci.getZip());
        intent.putExtra("COMPANY_CITY", ci.getCity());
        intent.putExtra("COMPANY_PHONE", ci.getPhone());
        intent.putExtra("COMPANY_HISTORY", ci.getHistory());
        intent.putExtra("COMPANY_IMAGE1", ci.getImage1());
        intent.putExtra("COMPANY_IMAGE2", ci.getImage2());
        intent.putExtra("COMPANY_IMAGE3", ci.getImage3());
        startActivity(intent);
        finish();
    }

    private void cancelEditCompanyInfo(View view)
    {
        Intent intent = new Intent(this, TabBarActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {

        int bId = v.getId();

        switch (bId)
        {
            case R.id.saveBtn:
                updateCompany(v);
                break;
            case R.id.cancelBtn:
                cancelEditCompanyInfo(v);
                break;
            case R.id.imageView1:
                selectFoto(R.id.imageView1);
                break;
            case R.id.imageView2:
                selectFoto(R.id.imageView2);
                break;
            case R.id.imageView3:
                selectFoto(R.id.imageView3);
                break;
        }
    }

    private void selectFoto(int imageViewId)
    {
        this.imageViewId = imageViewId;
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        Uri selectedImage = imageReturnedIntent.getData();
        String selectedImagePath = getRealPathFromURI(selectedImage);

        int imageViewId = this.imageViewId;
        switch (imageViewId) {
            case R.id.imageView1:
                setCompanyImagesView(companyImage1, selectedImagePath);
                break;
            case R.id.imageView2:
                setCompanyImagesView(companyImage2, selectedImagePath);
                break;
            case R.id.imageView3:
                setCompanyImagesView(companyImage3, selectedImagePath);
                break;
        }

        // Log.i("SELECTED IMAGE ", selectedImagePath);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
}
