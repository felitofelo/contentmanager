package com.metadoor.assigment3;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ShowCompanyLocation extends Activity implements AdapterView.OnItemClickListener, View.OnClickListener {

    private Button addNewButton;
    private ListView officesList;
    private ArrayList<Office> offices = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_company_location);

        officesList = (ListView) (findViewById(R.id.officesLv));
        officesList.setOnItemClickListener(this);
        offices = getOffices();

        List<Map<String, String>> data = getOfficesNames();
        SimpleAdapter adapter = new SimpleAdapter(
                this,
                data,
                android.R.layout.simple_list_item_2,
                new String[] {"address", "phone"},
                new int[] {android.R.id.text1, android.R.id.text2}
        );
        officesList.setAdapter(adapter);

        addNewButton = (Button) findViewById(R.id.addNewButton);
        addNewButton.setOnClickListener(this);
    }

    private ArrayList<Office> getOffices()
    {
        String[] projection = {
                DBConstants.LOCATION_ID,
                DBConstants.LOCATION_NAME,
                DBConstants.LOCATION_ADDRESS,
                DBConstants.LOCATION_ZIPCODE,
                DBConstants.LOCATION_CITY,
                DBConstants.LOCATION_PHONE
        };

        Cursor cursor = getContentResolver().query(
                DBConstants.CONTENT_URI_LOCATIONS,
                projection,
                null,
                null,
                null
        );

        int idIndex = cursor.getColumnIndex(DBConstants.LOCATION_ID);
        int nameIndex = cursor.getColumnIndex(DBConstants.LOCATION_NAME);
        int addressIndex = cursor.getColumnIndex(DBConstants.LOCATION_ADDRESS);
        int zipcodeIndex = cursor.getColumnIndex(DBConstants.LOCATION_ZIPCODE);
        int cityIndex = cursor.getColumnIndex(DBConstants.LOCATION_CITY);
        int phoneIndex = cursor.getColumnIndex(DBConstants.LOCATION_PHONE);

        if (cursor == null) throw new IllegalArgumentException("Office: NULL!");
        else if (cursor.getCount() < 1) throw new IllegalArgumentException("Nothing found!");
        else
        {
            while (cursor.moveToNext()) {
                Office office = new Office();
                int id = cursor.getInt(idIndex);
                String name = cursor.getString(nameIndex);
                String address = cursor.getString(addressIndex);
                String zip = cursor.getString(zipcodeIndex);
                String city = cursor.getString(cityIndex);
                String phone = cursor.getString(phoneIndex);

                office.setId(id);
                office.setName(name);
                office.setAddress(address);
                office.setZip(zip);
                office.setCity(city);
                office.setPhone(phone);

                offices.add(office);
            }
        }
        return offices;
    }

    private List<Map<String, String>> getOfficesNames() {
        List<Map<String, String>> data = new ArrayList<>();
        for (Office item : offices) {
            Map<String, String> address = new HashMap<>(2);
            address.put("address", item.getAddress());
            address.put("phone", item.getPhone());
            data.add(address);
        }
        return data;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String address = ((TextView)(view.findViewById(android.R.id.text1))).getText().toString();
        Office selectedOffice = getOfficeByAddress(address);
        editOffice(selectedOffice);
    }

    private Office getOfficeByAddress(String address) {
        Office office = null;
        for (Office item : offices) if(item.getAddress() == address) office = item;
        return office;
    }

    public void editOffice(Office selectedOffice) {
        Intent intent = new Intent(this, EditLocationActivity.class);
        intent.putExtra("id", selectedOffice.getId());
        intent.putExtra("name", selectedOffice.getName());
        intent.putExtra("address", selectedOffice.getAddress());
        intent.putExtra("zip", selectedOffice.getZip());
        intent.putExtra("city", selectedOffice.getCity());
        intent.putExtra("phone", selectedOffice.getPhone());
        startActivity(intent);
        finish();
    }

    public void newOffice() {
        Intent intent = new Intent(this, NewOfficeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.addNewButton) newOffice();
    }
}
