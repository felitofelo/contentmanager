package com.metadoor.assigment3;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class ShowCompanyInfo extends Activity implements View.OnClickListener {

    private CompanyInfo companyInfo;
    private TextView companyNameTxt;
    private TextView companyWebsiteTxt;
    private TextView companyAddressTxt;
    private TextView companyZipTxt;
    private TextView companyCityTxt;
    private TextView companyPhoneTxt;
    private TextView companyHistoryTxt;
    private ImageView companyImage1;
    private ImageView companyImage2;
    private ImageView companyImage3;
    private Button editInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_company_info);

        companyNameTxt = (TextView) findViewById(R.id.companyNameTxt);
        companyWebsiteTxt = (TextView) findViewById(R.id.companyWebsiteTxt);
        companyAddressTxt = (TextView) findViewById(R.id.companyAddressTxt);
        companyZipTxt = (TextView) findViewById(R.id.companyZipTxt);
        companyCityTxt = (TextView) findViewById(R.id.companyCityTxt);
        companyPhoneTxt = (TextView) findViewById(R.id.companyPhoneTxt);
        companyHistoryTxt = (TextView) findViewById(R.id.companyHistoryTxt);
        companyImage1 = (ImageView) findViewById(R.id.imageView1);
        companyImage2 = (ImageView) findViewById(R.id.imageView2);
        companyImage3 = (ImageView) findViewById(R.id.imageView3);

        companyInfo = getCompany();
        fillCompanyInfo(companyInfo);

        editInfo = (Button) findViewById(R.id.editInfoBtn);
        editInfo.setOnClickListener(this);
    }

    private void fillCompanyInfo(CompanyInfo companyInfo) {
        companyNameTxt.setText(companyInfo.getName());
        companyWebsiteTxt.setText(companyInfo.getWebsite());
        companyAddressTxt.setText(companyInfo.getAddress());
        companyZipTxt.setText(companyInfo.getZip());
        companyCityTxt.setText(companyInfo.getCity());
        companyPhoneTxt.setText(companyInfo.getPhone());
        companyHistoryTxt.setText(companyInfo.getHistory());


        if( (companyInfo.getImage1() == null)) setDefaultCompanyImage(companyImage1.getId());
        else setCompanyImagesView(companyImage1, companyInfo.getImage1());

        if( (companyInfo.getImage2() == null)) setDefaultCompanyImage(companyImage2.getId());
        else setCompanyImagesView(companyImage2, companyInfo.getImage2());

        if( (companyInfo.getImage3() == null)) setDefaultCompanyImage(companyImage3.getId());
        else setCompanyImagesView(companyImage3, companyInfo.getImage3());

    }

    private void setDefaultCompanyImage(int imageViewId){
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.noimage);
        String storageDirectory = getFilesDir().getAbsolutePath();
        File file = new File(storageDirectory, "noimage.PNG");

        try {
            FileOutputStream outStream = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
            System.out.println(new File(getFilesDir(), "noimage.png").getAbsolutePath());

            switch (imageViewId)
            {
                case R.id.imageView1:
                    companyInfo.setImage1(file.getAbsolutePath());
                    setCompanyImagesView(companyImage1, file.getAbsolutePath());
                    break;
                case R.id.imageView2:
                    companyInfo.setImage2(file.getAbsolutePath());
                    setCompanyImagesView(companyImage2, file.getAbsolutePath());
                    break;
                case R.id.imageView3:
                    companyInfo.setImage3(file.getAbsolutePath());
                    setCompanyImagesView(companyImage3, file.getAbsolutePath());
                    break;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void setCompanyImagesView(ImageView companyImage, String absolutePath) {
        File image = new File(absolutePath);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(),bmOptions);
        companyImage.setImageBitmap(bitmap);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_show_company_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private CompanyInfo getCompany()
    {
        CompanyInfo companyInfo;

        String[] projection = {
                DBConstants.COMPANY_ID,
                DBConstants.COMPANY_NAME,
                DBConstants.COMPANY_ADDRESS,
                DBConstants.COMPANY_CITY,
                DBConstants.COMPANY_HISTORY,
                DBConstants.COMPANY_PHONE,
                DBConstants.COMPANY_WEBSITE,
                DBConstants.COMPANY_ZIPCODE,
                DBConstants.IMAGE1,
                DBConstants.IMAGE2,
                DBConstants.IMAGE3,
        };

        String selectionClause = DBConstants.COMPANY_ID + " = ?";
        String[] selectionArgs = {"1"};

        Cursor cursor = getContentResolver().query(
                DBConstants.CONTENT_URI_INFO,
                projection,
                selectionClause,
                selectionArgs,
                null
        );

        int idIndex = cursor.getColumnIndex(DBConstants.COMPANY_ID);
        int nameIndex = cursor.getColumnIndex(DBConstants.COMPANY_NAME);
        int websiteIndex = cursor.getColumnIndex(DBConstants.COMPANY_WEBSITE);
        int addressIndex = cursor.getColumnIndex(DBConstants.COMPANY_ADDRESS);
        int zipcodeIndex = cursor.getColumnIndex(DBConstants.COMPANY_ZIPCODE);
        int cityIndex = cursor.getColumnIndex(DBConstants.COMPANY_CITY);
        int phoneIndex = cursor.getColumnIndex(DBConstants.COMPANY_PHONE);
        int historyIndex = cursor.getColumnIndex(DBConstants.COMPANY_HISTORY);
        int image1Index = cursor.getColumnIndex(DBConstants.IMAGE1);
        int image2Index = cursor.getColumnIndex(DBConstants.IMAGE2);
        int image3Index = cursor.getColumnIndex(DBConstants.IMAGE3);


        if (cursor == null) throw new IllegalArgumentException("Company info: NULL!");
        else if (cursor.getCount() < 1) throw new IllegalArgumentException("Nothing found!");
        else
        {
            companyInfo = new CompanyInfo();
            while (cursor.moveToNext()) {
                int id = cursor.getInt(idIndex);
                String name = cursor.getString(nameIndex);
                String website = cursor.getString(websiteIndex);
                String address = cursor.getString(addressIndex);
                String zip = cursor.getString(zipcodeIndex);
                String city = cursor.getString(cityIndex);
                String phone = cursor.getString(phoneIndex);
                String history = cursor.getString(historyIndex);
                String image1 = cursor.getString(image1Index);
                String image2 = cursor.getString(image2Index);
                String image3 = cursor.getString(image3Index);

                companyInfo.setId(id);
                companyInfo.setName(name);
                companyInfo.setWebsite(website);
                companyInfo.setAddress(address);
                companyInfo.setZip(zip);
                companyInfo.setCity(city);
                companyInfo.setPhone(phone);
                companyInfo.setHistory(history);
                companyInfo.setImage1(image1);
                companyInfo.setImage2(image2);
                companyInfo.setImage3(image3);
            }
        }

        return companyInfo;
    }

    private void showEditCompanyInfo(View view)
    {
        CompanyInfo ci = this.companyInfo;
        Intent intent = new Intent(this, EditCompanyInfo.class);
        intent.putExtra("COMPANY_NAME", ci.getName());
        intent.putExtra("COMPANY_WEBSITE", ci.getWebsite());
        intent.putExtra("COMPANY_ADDRESS", ci.getAddress());
        intent.putExtra("COMPANY_ZIP", ci.getZip());
        intent.putExtra("COMPANY_CITY", ci.getCity());
        intent.putExtra("COMPANY_PHONE", ci.getPhone());
        intent.putExtra("COMPANY_HISTORY", ci.getHistory());
        intent.putExtra("COMPANY_IMAGE1", ci.getImage1());
        intent.putExtra("COMPANY_IMAGE2", ci.getImage2());
        intent.putExtra("COMPANY_IMAGE3", ci.getImage3());
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        int bId = v.getId();
        switch (bId) {
            case R.id.editInfoBtn:
                showEditCompanyInfo(v);
                break;
        }
    }
}
